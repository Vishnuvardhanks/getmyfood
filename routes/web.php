<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

 
Route::get('/usercontroller/path',[
   'middleware' => 'First',
   'uses' => 'UserController@showPath'
]);

Route::get('/register',function(){
   return view('register');
});
Route::post('/user/login', array('uses'=>'UserController@login')



	);

Route::get('/user/logout', array('uses'=>'UserController@logout')



	);


Route::post('/user/register', array('uses'=>'UserController@postRegister')




	);

Route::get('/poducts', 'UserController@printProducts');
Route::get('/cartItems','UserController@getCarItems');

 Route::any('/user/addToCart', 'UserController@addToCart');

