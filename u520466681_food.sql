
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 01, 2017 at 07:13 PM
-- Server version: 10.0.28-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u520466681_food`
--

-- --------------------------------------------------------

--
-- Table structure for table `deliver_address`
--

CREATE TABLE IF NOT EXISTS `deliver_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `deliver_address`
--

INSERT INTO `deliver_address` (`address_id`, `user_id`, `address`) VALUES
(1, 6, 'asdas'),
(2, 7, 'anantapur'),
(3, 8, 'kadapa');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_first_name` varchar(100) NOT NULL,
  `user_last_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_mobile` varchar(100) NOT NULL,
  `user_password` varchar(1000) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first_name`, `user_last_name`, `user_email`, `user_mobile`, `user_password`) VALUES
(1, 'nikhil', 'ramavath', 'ramavathnikhil@gmail.com', '8099504528', 'nikhil'),
(2, 'ni', 'ra', 'ramavathnikhils@gmail.com', '8099504528', 'nikhil'),
(3, 'a', 'A', 'U@U.com', '8099504528', '123'),
(4, 'Nikhil', 'rad', 'a@a.com', '9000912213', '123'),
(5, 'manoj', 'reddy', 'manoj@manoj.com', '9000912213', '123'),
(6, 'a', 'a', 'g@g.com', '8099504528', '123'),
(7, 'vishnu', 'ks', 'Vishnuvardhankonanki@gmail.com', '9000912213', '123456'),
(8, 'siva', 'krishna', 'shiva@gmail.com', '8099271293', 'shiva');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
