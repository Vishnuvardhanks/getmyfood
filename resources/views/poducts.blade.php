<html>
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>welcome | GetMyFood</title>
        
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
       
 <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
        <style>
            .carousel-inner > .item > img,
            .carousel-inner > .item > a > img {
                width: 100%;
                margin: auto;
                height: 60vh;
            }
        </style>

        

        <script>

        $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

function addToCart(i) {
     

     var quantity=document.getElementById("product"+i).value;
     var price=document.getElementById("price"+i).innerHTML;
     var name=document.getElementById("name"+i).innerHTML;
 

 

        if(quantity<=0)
{

alert("Please enter some valid quantity");
}



else
{
            $.ajax({
    method: 'POST', // Type of response and matches what we said in the route
    url: '/user/addToCart', // This is the url we gave in the route
    data: {'id' : i,'quantity': quantity,'name':name,'price':price}, // a JSON object to send back
    success: function(response){ // What to do if we succeed
         alert("Item added to cart");
    },
    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
        console.log(JSON.stringify(jqXHR));
         alert(JSON.stringify(jqXHR));
    }
});

      }
}
</script>   
    </head>
    <body>
        <div class="container">
        <div class="top_header">

            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="brand_section">
                        <h1>GetMyFood</h1>
                    </div>
                </div>

                <?php 
               if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
                if(empty($_SESSION["login_email"]))
                {
                  ?>
                  <div class="col-sm-6 col-md-6">
                    <div class="log_reg">
                        <button type="button" class="login_btn">Login</button>
                        <button type="button" class="reg_btn">Register</button>
                    </div>

                </div>  

                    <?php
                }
                ?>
            </div>
        </div>
        <div class="navgation_section">
        <ul>
            <li><a href = "/">Home</a></li>
            <li><a>About</a></li>
           
            <?php 
            
            if(!empty($_SESSION["login_email"]))
            {
              ?>
           <li><a href="/poducts">Menu</a></li>


             <?php
          }
          ?>
            <li><a href="/cartItems">Cart</a></li>
            <?php 
            
            if(!empty($_SESSION["login_email"]))
            {
              ?>
              <li><a href = "/user/logout"> Logout</a></li>

              <?php
          }
          ?>
      </ul>
  </div>
  <div class="row">
    <div class="col-sm-12">
    
 @if(isset($products))
    <table class="table table-responsive">
      <thead>
        <tr>
          <th>Product</th>
          <th>Name</th>
          <th>Price</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      
     
        @foreach($products as $product)
 
        <tr>
          <td><img height="100" width="100" src= "{{$product->product_image}} "alt="Food"></td>
          <td ><p id="name{{$product->id}}">{{$product->product_name}}</p></td>
          <td ><p id="price{{$product->id}}">${{$product->product_price}}</p></td>
          <td><input class="btn btn-success" placeholder="Please enter Quanatity" id="product{{$product->id}}" type="number" min="1"></input><button style="width: auto"   class="btn btn-danger" onclick="addToCart({{$product->id}})" ">Add to cart</button></td>
          <td></td>
        </tr>

       

        @endforeach

          </tbody>
    </table>
         @endif


          @if(isset($user_products))

 <table class="table table-bordered" style="margin-top: 20px;">
          <thead>
            <th>Item name</th>
            <th>No.of Items</th>
            <th>Item cost</th>
            <th>Total Item cost</th>
            
          </thead>
          <tbody>

<?php $total_count =0;?>

            @foreach($user_products as $products)

     <?php   $product = explode(",", $products);?>
            <tr>
              <td>{{$product[3]}}</td>
              <td>{{$product[1]}}</td>
              <td>{{$product[2]}}</td>
              <td>{{$product[1] * ltrim($product[2], '$')}}</td>  </tr>

<?php $total_count = $total_count+ ($product[1] * ltrim($product[2], '$'));?>
              @if ($products == end($user_products)) 

              <tr>
              <td></td>
              <td></td>
              <td>Grand Total</td>
              <td>{{$total_count}}</td>
               </tr>

              @endif>
     
  

              @endforeach
          </tbody>
        </table>


            @endif

    
    </div>
  </div>
  </div>
  <div id="cartModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content cart">
      <div class="modal-header">
        <h4 class="modal-title">Your cart</h4>
      </div>
      <div class="modal-body">
        <table class="table table-border">
          <thead>
            <th>Item name</th>
            <th>No.of Items</th>
            <th>Item cost</th>
            <th>Total cost</th>
            <th></th>
          </thead>
          <tbody>
            <tr>
              <td>Biryani</td>
              <td>2</td>
              <td>180</td>
              <td>360</td>

            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    </body>
</html>