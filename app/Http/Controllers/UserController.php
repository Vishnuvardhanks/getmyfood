<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class UserController extends Controller
{
    //
	public function __construct(){

	}

	
	public function showPath(Request $request){
		$uri = $request->path();
		echo '<br>URI: '.$uri;
		
		$url = $request->url();
		echo '<br>';
		
		echo 'URL: '.$url;
		$method = $request->method();
		echo '<br>';
		
		echo 'Method: '.$method;
	}


	public function logout(Request $request)
	{
		session_start();
		unset($_SESSION["login_email"]);
	 $this->goingBack("You have been logged out");

		//return View("welcome");
	}
	public function login(Request $request)
	{
		$email = $request->email;
		$password = $request->password;

		if(empty($email))
		{
			$this->goingBack("Please enter valid email id");
		}
		else
		{
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$this->goingBack("Please enter valid email id");
			}

			else
			{
				if(empty($password))
				{
					$this->goingBack("Please enter password");
				}
				else
				{
					/*$json_data = file_get_contents('registration_details.txt');
					$data = json_decode($json_data, true);

					if(!$this->is_array_empty($data))
					{
						$this->goingBack("Please register to continue")	;
					}
					else
					{

						foreach ($data as $user) {

							if($user['email'] == $email)
							{
								if($user['password'] == $password)
								{
									session_start();
									$_SESSION["login"] = true;
									$_SESSION["login_email"]  = $email;
									//$this->goingBack("Login successfull")	;

									$this->goingBack("Login successfull");
									return;
								}
								else
								{
									$this->goingBack("Wrong Password")	;
									return;
								}
							}
						}
						$this->goingBack("User doesnt exists")	;

					}
*/

					$users = DB::select("select * from users where user_email = '".$email."' and user_password = '".$password."'");

					if(sizeof($users)>0)
					{
						session_start();
						$_SESSION["login"] = true;
						$_SESSION["login_email"]  = $email;
						 
									//$this->goingBack("Login successfull")	;

						$this->goingBack("Login successfull");
						return;
					}
					else
					{
						$this->goingBack("email/password is wrong")	;
					}


				}
			}

		}
	}


/*
 return true if the array is not empty
 return false if it is empty
*/
 function is_array_empty($arr){
 	if(is_array($arr)){     
 		foreach($arr  as $key => $value){
 			if(!empty($value) || $value != NULL || $value != ""){
 				return true;
              break;//stop the process we have seen that at least 1 of the array has value so its not empty
          }
      }
      return false;
  }
}

public function postRegister(Request $request){
      //Retrieve the name input field
	$first_name = $request->input('first_name');
	$last_name = $request->last_name;
	$email = $request->email;
	$mobile = $request->mobile;
	$password = $request->password;
	$confirm_password = $request->confirm_password;
	$deliver_address = $request->deliver_address;

	if(empty($first_name))
	{

		$this->goingBack("Please enter First Name");

	}

	else
	{

		if(empty($last_name))
		{

			$this->goingBack("Please enter Last Name<br/>");

		}

		else
		{

			if(empty($email))
			{
				$this->goingBack("Please enter Email"); 
			}

			else
			{

				if(empty($mobile))
				{
					$this->goingBack("Please enter Mobile");
				}

				else
				{
					if(empty($password))
					{
						$this->goingBack("Please enter password");
					}

					else
					{

						if(empty($confirm_password))
						{
							$this->goingBack("Please enter Confirm password");
						}

						else
						{


    // check if e-mail address is well-formed
							if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
								$this->goingBack("Please enter valid email id");
							}

							else
							{
if(preg_match('/^\d{10}$/',$mobile)) // phone number is valid
{

	if($password == $confirm_password)
	{

		/*$myFile = "registration_details.txt";
		$user_info=array("first_name"=>$first_name,"last_name"=>$last_name,"email"=>$email,"mobile"=>$mobile,"password"=>$password);
		$data=array();
		if (file_exists($myFile)) {
			$data=json_decode(file_get_contents($myFile),true);	
		}
		$data[]=$user_info;
		file_put_contents($myFile, json_encode($data));*/



		if(empty($deliver_address))
		{
			$this->goingBack("Please enter delivery address");
		}

		else
		{
			$users = DB::select("select * from users where user_email = '".$email."'");


			if(sizeof($users)>0)
			{
				$this->goingBack("User already exists");
			}

			else
			{


				$id =  DB::table('users')->insertGetId(
					['user_first_name' => $first_name, 'user_last_name' => $last_name,'user_email' => $email,'user_mobile' => $mobile,'user_password' =>$password]
					);


				$id =  DB::table('deliver_address')->insertGetId(
					['user_id' => $id, 'address' => $deliver_address]
					);



				echo "You have registered successfully<br/>";
				echo '<a href = "/">Click Here</a> to go back.';

			}


		}

	}
	else
	{

		$this->goingBack("Password and confirm password should be same");
	}





}
    else // phone number is not valid
    {

    	$this->goingBack("Phone number invalid");
    }




}
}
}
}
}
}


}










     /* echo "Record inserted successfully.<br/>";
     echo '<a href = "/insert">Click Here</a> to go back.';*/


 }



public function printProducts()
{
	$products = DB::table("products")->get();


	if(sizeof($products)>0)
	{
  
 return View('poducts')->with('products', $products);
 
	}
	else
	{
     return View('poducts');
	}

}

 public function goingBack($message)
 {
 	echo '<script language="javascript">';
 	echo 'alert("'.$message.'");  window.history.back();';
 	echo '</script>';
 }


 public function addToCart(){

session_start();

if(isset($_SESSION["login_email"]))
{

$var = $_SESSION["login_email"];
 
   
     $id = $_POST['id']; 
     $quantity=$_POST['quantity'];
     $price =$_POST['price'];
     $name = $_POST['name'];
   
     $item = $id.",".$quantity.",".$price.",".$name;
     $cookie_name = "cart_cookies";
     /* $id =  DB::table('cart')->insertGetId(
					['item_id' => $id, 'user_id' => $var,'quantity' => $quantity] );*/

if(!isset($_COOKIE[$cookie_name])) {
    
    $item="s".$item;
     setcookie($cookie_name, $item, time() + (86400 * 30),"/cartItems");
} else {
     
 /*    $cart = array();
array_merge($cart, unserialize($_COOKIE[$cookie_name]));		
*/

$item = $_COOKIE[$cookie_name]."+".$item;
		 
      
     setcookie($cookie_name,$item, time() + (86400 * 30),"/cartItems");

}

}

 

   }


   public function getCarItems()
   {

session_start();
if(isset($_SESSION["login_email"]))
{


$cookie_name = "cart_cookies";


$var = $_SESSION["login_email"];

if(!isset($_COOKIE[$cookie_name])) {

    echo '<script language="javascript">';
 	echo 'alert("no cokkie");  window.history.back();';
 	echo '</script>';
}
else
{

 $cookie_name = "cart_cookies";
 $products = explode('+', $_COOKIE[$cookie_name]);
    
    if(sizeof($products)>0)
	{
  
  //session_destroy();
 return View('poducts')->with('user_products', $products);
 
	}
	else
	{
		//session_destroy();
     return View('poducts');
	}

}

     

}

 
  /* $products = DB::select("SELECT cart.quantity,products.product_name,products.product_price,(cart.quantity * products.product_price) as total_price,(SELECT sum(cart.quantity * products.product_price) FROM cart JOIN products WHERE  user_id = '".$var."' and products.id = cart.item_id) as count FROM `cart` JOIN products WHERE user_id = '".$var."' and products.id = cart.item_id");
*/





			
   }

}


